var app = new Vue({
  el: '#app',
  data: {
    panels: [
      { text: 'Uno', isActive: false },
      { text: 'Dos', isActive: false },
      { text: 'Tres', isActive: false },
      { text: 'Cuatro', isActive: false },
      { text: 'Cinco', isActive: false }
    ],
    img: -1
  },
  methods: {
    expandir(index) {
      /* First click */
      if (this.img < 0) {
        this.$set(this.panels[index], 'isActive', true)
        this.img = index;
        return
      }
      /* The rest of clicks */
      this.$set(this.panels[this.img], 'isActive', false)
      this.$set(this.panels[index], 'isActive', true)
      this.img = index;
    }
  }
})
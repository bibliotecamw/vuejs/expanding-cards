# Expansive cards

Es una sucesión de imágenes recortadas.
Cuando se clickea en una imagen, se expande y se muestra completa.

## Contenido:

VueJS:
    v-for, objetos, $set, @click, v-show

css:
    flex, animation, radial-gradient

    flex: none | [ <'flex-grow'> <'flex-shrink'>? || <'flex-basis'> ]

flex-grow: 
    It defines the ability for a flex item to grow if necessary.
    For example, if all items have flex-grow set to 1, every child will set to an equal size inside the container. If you were to give one of the children a value of 2, that child would take up twice as much space as the others.

flex-shrink:
    It specifies the “flex shrink factor”, which determines how much the flex item will shrink relative to the rest of the flex items in the flex container when there isn’t enough space on the row.

flex-basis:
    It specifies the initial size of the flex item, before any available space is distributed according to the flex factors.

Example:

flex: 5;
    flex-grow: 5;
    flex-shrink: 1;
    flex-basis: 0%;